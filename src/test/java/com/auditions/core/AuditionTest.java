package com.auditions.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;


@RunWith(Parameterized.class)
public class AuditionTest {
	
	
private Auditions audition;
private String expectedvalue;

public AuditionTest(String expectedvalue,Auditions audition)
{
this.expectedvalue= expectedvalue;
this.audition=audition;
}
	
@Parameterized.Parameters
public static Collection cost()
{
	return Arrays.asList(new Object[][] {
		{"324-Performer", (new Performer(324,"Performer"))},
		{"1212-Performer",(new Performer(1212,"Performer"))},
		{"121-Performer",(new Performer(121,"Performer"))},
		{"1121-Performer",(new Performer(1121,"Performer"))},
		{"tap-324-Dancer",(new Dancer(324,"Dancer","tap"))},
		{"salsa-978-Dancer",(new Dancer(978,"Dancer","salsa"))},
		{"I sing in the key of-G-324",(new Vocalist(324,"I sing in the key of","G"))}
		
		
		});
}
	

	@Test
	public void AuditionTesting() {
	
		
		String actual=audition.performance();
		
		assertEquals(expectedvalue, actual);
	}


	

}
