package com.auditions.core;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.internal.runners.statements.ExpectException;
import org.junit.rules.ExpectedException;


public class VocalistTest {



	private Vocalist v;


	@Before
	public void init() {

		v= new Vocalist(324,"I sing in the key of","G");


	}



	@Test
	public void performTest() {

		String actual=v.performance();
		String expected= "I sing in the key of-G-324";
		assertEquals(expected, actual);

	}

	@Test
	public void performVocalistVolumeTest()
	{

		//"I sing in the key of"+"-"+key+"-"+"at the volume "+volume+"-"+unionId;
		String actual= v.performance(5);
		String expected="I sing in the key of-G-at the volume 5-324";
		assertEquals(expected, actual);
	}
	
	@Rule public ExpectedException thrown= ExpectedException.none();
	@Test
	public void volumeTest()
	
	{
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("Volume should be in range 1-10");
		 v.performance(12);
		
		
	}


}

