package com.auditions.core;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DancerTest {

private Dancer d;

	
	@Before
	public void init() {
	
		d= new Dancer(324,"Dancer","tap");
		
	
	}
	

	
	@Test
	public void performTest() {
		
		String actual=d.performance();
		String expected= "tap-324-Dancer";
		assertEquals(expected, actual);
		
	}
}
