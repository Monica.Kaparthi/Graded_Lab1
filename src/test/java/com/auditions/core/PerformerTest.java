package com.auditions.core;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PerformerTest {
	
	private Performer p;

	
	@Before
	public void init() {
	
		p= new Performer(324,"Performer");
		
	
	}
	

	
	@Test
	public void performTest() {
		
		String actual=p.performance();
		String expected= "324-Performer";
		assertEquals(expected, actual);
		
	}

}
