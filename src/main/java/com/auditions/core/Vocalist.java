package com.auditions.core;

public  class Vocalist extends Auditions {
	
	
	String key=null;
	

	public Vocalist(int unionId, String type,String key) {
		super(unionId, type);
		this.key=key;
		
		
	}
	

	@Override
	public String performance() {
		
		String s=type+"-"+key+"-"+unionId;
		
		return s;
	}
	
	
	//Method for singers with volume
	public String performance(int volume)
	{
		String s1=null;
		if(volume<=0 || volume>=11)
		{
			
			throw new IllegalArgumentException("Volume should be in range 1-10");
		}
		else
		{
		
		s1="I sing in the key of"+"-"+key+"-"+"at the volume "+volume+"-"+unionId;
		}
		return s1;
		
	}


	

}
