package com.auditions.core;

public class Dancer extends Auditions {

	
	String style=null;
	
	public Dancer(int unionId, String type,String style) {
		super(unionId, type);
		
		
	  this.style=style;
	}

	@Override
	public String performance() {
		
		String dance= style+"-"+unionId+"-"+type;
		
		return dance;
	}

	

}
